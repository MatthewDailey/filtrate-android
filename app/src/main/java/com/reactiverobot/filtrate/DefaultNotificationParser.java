package com.reactiverobot.filtrate;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.service.notification.StatusBarNotification;

public class DefaultNotificationParser implements NotificationParser {

    @Override
    public FiltrateNotification parse(StatusBarNotification statusBarNotification) {
        String packageName = statusBarNotification.getPackageName();

        Notification notification = statusBarNotification.getNotification();

        String ticker = "";
        if (notification.tickerText != null) {
            ticker = notification.tickerText.toString();
        }

        String title = "";
        String content = "";
        Bundle extras = notification.extras;
        if (extras != null) {
            title = extras.getString("android.title").toString();
            content = extras.getString("android.text").toString();
        }

        return new FiltrateNotification(packageName, title, content, ticker);
    }

}
