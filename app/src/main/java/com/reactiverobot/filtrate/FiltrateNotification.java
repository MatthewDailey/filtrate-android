package com.reactiverobot.filtrate;

import android.app.NotificationManager;
import android.content.Context;

public class FiltrateNotification {

    public class NotificationText {
        private final String packageName;
        private final String title;
        private final String content;
        private final String ticker;

        public NotificationText(String packageName, String title, String content, String ticker) {
            this.packageName = packageName;
            this.title = title;
            this.content = content;
            this.ticker = ticker;
        }

        public boolean contains(String s) {
            return title.contains(s)
                    || content.contains(s)
                    || ticker.contains(s);
        }
    }

    private final NotificationText text;

    public FiltrateNotification(String packageName, String title, String content, String ticker) {
        this.text = new NotificationText(packageName, title, content, ticker);
    }

    public NotificationText text() {
        return text;
    }

}
