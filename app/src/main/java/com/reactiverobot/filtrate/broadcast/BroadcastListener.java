package com.reactiverobot.filtrate.broadcast;

public interface BroadcastListener {
    void handle(String message);
}
