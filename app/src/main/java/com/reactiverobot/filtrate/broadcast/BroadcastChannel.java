package com.reactiverobot.filtrate.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

public class BroadcastChannel {

    public final static String MESSAGE_EXTRA = "message";

    private final Context context;
    private final String channel;

    public BroadcastChannel(Context context, String channel) {
        this.context = context;
        this.channel = channel;
    }

    public void send(String message) {
        Intent broadcastIntent = new Intent(channel);
        broadcastIntent.putExtra(MESSAGE_EXTRA, message);
        LocalBroadcastManager.getInstance(context).sendBroadcast(broadcastIntent);
    }

    public void listen(final BroadcastListener listener) {
        LocalBroadcastManager.getInstance(context).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        String message = intent.getStringExtra(MESSAGE_EXTRA);
                        listener.handle(message);
                    }
                },
                new IntentFilter(channel));
    }

}
