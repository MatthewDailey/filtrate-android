package com.reactiverobot.filtrate.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;

public class FiltratePrefs {

    private static final String FILTRATE_PREFS_FILE = "filtrate.prefs";
    private static final String USERNAME_PREF = "username";
    private static final String ENABLED_PREF = "enabled";
    private static final String PREVIOUS_RINGER_PREF = "previous_ringer";

    private final Context context;

    public FiltratePrefs(Context context) {
        this.context = context;
    }

    public boolean enabled() {
        return getFiltratePrefs().getBoolean(ENABLED_PREF, false);
    }

    public void enabled(boolean enabled) {
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        if (enabled) {
            previousRinger(audioManager.getRingerMode());
            audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
        } else {
            audioManager.setRingerMode(previousRinger());
        }
        getFiltratePrefs().edit().putBoolean(ENABLED_PREF, enabled).apply();
    }

    public String username() {
        return getFiltratePrefs().getString(USERNAME_PREF, "");
    }

    public void username(String username) {
        getFiltratePrefs().edit().putString(USERNAME_PREF, username).apply();
    }

    private void previousRinger(int previousRinger) {
        getFiltratePrefs().edit().putInt(PREVIOUS_RINGER_PREF, previousRinger).apply();
    }

    private int previousRinger() {
        return getFiltratePrefs().getInt(PREVIOUS_RINGER_PREF, AudioManager.RINGER_MODE_SILENT);
    }

    private SharedPreferences getFiltratePrefs() {
        return context.getSharedPreferences(FILTRATE_PREFS_FILE, Context.MODE_PRIVATE);
    }
}
