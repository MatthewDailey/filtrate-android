package com.reactiverobot.filtrate;

import android.content.Context;
import android.service.notification.StatusBarNotification;

public interface NotificationParser {
    FiltrateNotification parse(StatusBarNotification statusBarNotification);
}
