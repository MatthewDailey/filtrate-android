package com.reactiverobot.filtrate;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.content.Context;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import com.reactiverobot.filtrate.prefs.FiltratePrefs;

@SuppressLint("OverrideAbstract")
public class NotificationFilteringService extends NotificationListenerService {
    
    private final NotificationParser notificationParser;

    public NotificationFilteringService() {
        this(new DefaultNotificationParser());
    }

    public NotificationFilteringService(NotificationParser notificationParser) {
        this.notificationParser = notificationParser;
    }

    @Override
    public void onNotificationPosted(StatusBarNotification statusBarNotification) {
        FiltratePrefs prefs = new FiltratePrefs(this);
        if (prefs.enabled()) {
            FiltrateNotification notification = notificationParser.parse(statusBarNotification);

            if (!notification.text().contains("@" + prefs.username())) {
                cancelNotification(statusBarNotification.getKey());
            }
        }
    }

}

