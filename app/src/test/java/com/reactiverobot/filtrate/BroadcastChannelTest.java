package com.reactiverobot.filtrate;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.reactiverobot.filtrate.broadcast.BroadcastChannel;
import com.reactiverobot.filtrate.broadcast.BroadcastListener;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;

import java.math.BigInteger;
import java.security.SecureRandom;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, emulateSdk = 21)
public class BroadcastChannelTest {

    private Mockery mock;
    private Context context;
    private String message;
    private String channelName;

    private SecureRandom random = new SecureRandom();

    @Before
    public void setup() {
        mock = new Mockery();
        context = new ShadowActivity().getApplicationContext();
        message = new BigInteger(130, random).toString(32);
        channelName = new BigInteger(130, random).toString(32);
    }

    @Test
    public void testSendMessage() {
        BroadcastChannel channel = new BroadcastChannel(context, channelName);

        channel.send(message);
    }

    @Test
    public void testReceiveMessage() {
        BroadcastChannel channel = new BroadcastChannel(context, channelName);

        final BroadcastListener listener = this.mock.mock(BroadcastListener.class);

        mock.checking(new Expectations() {{
            oneOf(listener).handle(message);
        }});

        channel.listen(listener);
        channel.send(message);

        mock.assertIsSatisfied();
    }

    @Test
    public void testReceiveBroadcastMessage() {
        BroadcastChannel channel = new BroadcastChannel(context, channelName);

        final BroadcastListener listener = this.mock.mock(BroadcastListener.class);
        channel.listen(listener);
        mock.checking(new Expectations() {{
            oneOf(listener).handle(message);
        }});

        Intent broadcastIntent = new Intent(channelName);
        broadcastIntent.putExtra(BroadcastChannel.MESSAGE_EXTRA, message);
        LocalBroadcastManager.getInstance(context).sendBroadcast(broadcastIntent);

        mock.assertIsSatisfied();
    }

    @Test
    public void testSendMessageBetweenChannels() {
        BroadcastChannel receiver = new BroadcastChannel(context, channelName);
        BroadcastChannel sender = new BroadcastChannel(context, channelName);

        final BroadcastListener listener = this.mock.mock(BroadcastListener.class);
        receiver.listen(listener);
        mock.checking(new Expectations() {{
            oneOf(listener).handle(message);
        }});

        sender.send(message);

        mock.assertIsSatisfied();
    }
}