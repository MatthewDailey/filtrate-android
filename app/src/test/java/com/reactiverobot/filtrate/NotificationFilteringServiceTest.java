package com.reactiverobot.filtrate;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Parcel;
import android.os.UserHandle;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.support.annotation.NonNull;

import com.reactiverobot.filtrate.prefs.FiltratePrefs;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;
import org.robolectric.shadows.ShadowNotificationManager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, emulateSdk = 21)
public class NotificationFilteringServiceTest {

    private final int notificationId = 1;
    private final String packageName = "pkg";
    private final String title = "title";
    private final String content = "content";
    private final String ticker = "ticker";

    private Mockery mockery;
    private ShadowApplication application;
    private Context context;

    @Before
    public void setup() {
        this.mockery = new Mockery() {{
            setImposteriser(ClassImposteriser.INSTANCE);
        }};
        this.application = new ShadowApplication();
        this.context = Robolectric.setupActivity(MainActivity.class);
    }

    @Test
    public void testFiltrateNotificationFromStatusBarNotification() {
        FiltrateNotification notification = new DefaultNotificationParser()
                .parse(getDefaultTestStatusBarNotification());
        assertFalse(notification.text().contains(packageName));
        assertTrue(notification.text().contains(title));
        assertTrue(notification.text().contains(content));
        assertTrue(notification.text().contains(ticker));
    }

    @Test
    public void testFiltrateNotificationFromStatusBarNotificationWithCustomPackage() {
        String expectedTitle = "com.reactiverobot title";
        FiltrateNotification notification = new DefaultNotificationParser().parse(getTestStatusBarNotification(
                packageName, expectedTitle, content, ticker));
        assertTrue(notification.text().contains(expectedTitle));
    }

    @Test
    public void testFiltrateNotificationFromStatusBarNotificationWithCustomValues() {
        String expectedPackageName = "com.reactiverobot";
        String expectedTitle = "FiltrateApp";
        String expectedContent = "cool content";
        String expectedTicker = "tick tock";
        FiltrateNotification notification = new DefaultNotificationParser().parse(getTestStatusBarNotification(
                expectedPackageName, expectedTitle, expectedContent, expectedTicker));
        assertFalse(notification.text().contains(expectedPackageName));
        assertTrue(notification.text().contains(expectedTitle));
        assertTrue(notification.text().contains(expectedContent));
        assertTrue(notification.text().contains(expectedTicker));
    }

    @Test
    public void testInjectDependenciesToNotificationFilteringService() {
        new NotificationFilteringService(mockery.mock(NotificationParser.class));

        mockery.assertIsSatisfied();
    }

    @Test
    public void testOnlyParseNotificationIfEnabled() {
        final NotificationParser notificationParser = mockery.mock(NotificationParser.class);
        final NotificationFilteringService notificationFilteringService = new NotificationFilteringService(notificationParser);

        FiltratePrefs filtratePrefs = new FiltratePrefs(notificationFilteringService.getApplicationContext());
        filtratePrefs.enabled(true);

        final StatusBarNotification statusBarNotification = getDefaultTestStatusBarNotification();

        mockery.checking(new Expectations(){{
            oneOf(notificationParser).parse(statusBarNotification);
        }});

        notificationFilteringService.onNotificationPosted(statusBarNotification);

        mockery.assertIsSatisfied();
    }

    @Test
    public void testClearIfTitleContainsAtNameAndEnabled() {
        final NotificationParser notificationParser = mockery.mock(NotificationParser.class);
        final FiltrateNotification filtrateNotification = mockery.mock(FiltrateNotification.class);
        final FiltrateNotification.NotificationText notificationText = mockery.mock(
                FiltrateNotification.NotificationText.class);
        final StatusBarNotification statusBarNotification = mockery.mock(StatusBarNotification.class);

        final NotificationFilteringService notificationFilteringService = new NotificationFilteringService(notificationParser);
        mockery.checking(new Expectations(){{
            oneOf(notificationParser).parse(statusBarNotification);
            will(returnValue(filtrateNotification));

            oneOf(filtrateNotification).text();
            will(returnValue(notificationText));

            oneOf(notificationText).contains("@matt");
            will(returnValue(true));

        }});

        FiltratePrefs filtratePrefs = new FiltratePrefs(notificationFilteringService.getApplicationContext());
        filtratePrefs.enabled(true);
        filtratePrefs.username("matt");

        notificationFilteringService.onNotificationPosted(statusBarNotification);

        mockery.assertIsSatisfied();
    }

    private StatusBarNotification getDefaultTestStatusBarNotification(){
        return getTestStatusBarNotification(packageName, title, content, ticker);
    }

    private StatusBarNotification getTestStatusBarNotification(String packageName, String title,
                                                               String content, String ticker) {
        Parcel parcel = Parcel.obtain();
        parcel.writeInt(1);
        UserHandle userHandle = new UserHandle(parcel);

        Notification notification = getTestNotification(title, content, ticker);
        return new StatusBarNotification(packageName, "opPkg", 0 /* id */, "tag", 0 /* uid */,
                0 /*initialPid */, 0 /*score*/, notification, userHandle, 0 /*postTime*/);
    }

    @NonNull
    private Notification getTestNotification(String title, String content, String ticker) {
        Bundle bundle = new Bundle();
        bundle.putString("android.text", content);
        bundle.putString("android.title", title);

        Notification notification = new Notification();
        notification.extras = bundle;
        notification.tickerText = ticker;
        return notification;
    }


}
