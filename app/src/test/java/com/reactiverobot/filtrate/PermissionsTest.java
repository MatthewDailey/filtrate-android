package com.reactiverobot.filtrate;

import android.app.Application;
import android.content.pm.PackageManager;

import com.reactiverobot.filtrate.BuildConfig;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowApplication;

import static org.junit.Assert.assertEquals;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, emulateSdk = 21)
public class PermissionsTest {

    @Test
    public void testRobolectricPermissions() {
        Application context = new ShadowActivity().getApplicationContext();
        assertEquals(PackageManager.PERMISSION_DENIED,
                context.checkCallingOrSelfPermission("android.permission.BIND_NOTIFICATION_LISTENER_SERVICE"));
    }

    @Test
    public void testRobolectricGrantPermissions() {
        ShadowApplication shadowApplication = new ShadowApplication();
        assertEquals(PackageManager.PERMISSION_DENIED,
                shadowApplication.checkCallingOrSelfPermission("android.permission.BIND_NOTIFICATION_LISTENER_SERVICE"));
        shadowApplication.grantPermissions("android.permission.BIND_NOTIFICATION_LISTENER_SERVICE");
        assertEquals(PackageManager.PERMISSION_GRANTED,
                shadowApplication.checkCallingOrSelfPermission("android.permission.BIND_NOTIFICATION_LISTENER_SERVICE"));
    }
}
