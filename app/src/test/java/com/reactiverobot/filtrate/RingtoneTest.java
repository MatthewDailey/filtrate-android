package com.reactiverobot.filtrate;

import android.content.Context;
import android.media.AudioManager;
import android.view.View;

import com.reactiverobot.filtrate.BuildConfig;
import com.reactiverobot.filtrate.MainActivity;
import com.reactiverobot.filtrate.R;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import java_cup.Main;

import static org.junit.Assert.assertEquals;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, emulateSdk = 21)
public class RingtoneTest {

    @Test
    public void testVerifyRobolectricAudioManager() {
        MainActivity mainActivity = Robolectric.setupActivity(MainActivity.class);
        AudioManager audioManager = (AudioManager) mainActivity.getSystemService(Context.AUDIO_SERVICE);
        assertEquals(AudioManager.RINGER_MODE_NORMAL, audioManager.getRingerMode());
    }

    @Test
    public void testWhenEnabledRingerModeIsSilent() {
        MainActivity mainActivity = Robolectric.setupActivity(MainActivity.class);
        AudioManager audioManager = (AudioManager) mainActivity.getSystemService(Context.AUDIO_SERVICE);

        mainActivity.findViewById(R.id.filter_enabled).performClick();

        assertEquals(AudioManager.RINGER_MODE_SILENT, audioManager.getRingerMode());
    }

    @Test
    public void testWhenEnableThenDisabledRingerModeIsNormal() {
        MainActivity mainActivity = Robolectric.setupActivity(MainActivity.class);
        AudioManager audioManager = (AudioManager) mainActivity.getSystemService(Context.AUDIO_SERVICE);

        mainActivity.findViewById(R.id.filter_enabled).performClick();
        mainActivity.findViewById(R.id.filter_enabled).performClick();

        assertEquals(AudioManager.RINGER_MODE_NORMAL, audioManager.getRingerMode());
    }

    @Test
    public void testWhenEnableThenDisabledRingerModeIsReset() {
        MainActivity mainActivity = Robolectric.setupActivity(MainActivity.class);
        AudioManager audioManager = (AudioManager) mainActivity.getSystemService(Context.AUDIO_SERVICE);
        audioManager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);

        mainActivity.findViewById(R.id.filter_enabled).performClick();
        mainActivity.findViewById(R.id.filter_enabled).performClick();

        assertEquals(AudioManager.RINGER_MODE_VIBRATE, audioManager.getRingerMode());
    }
}
