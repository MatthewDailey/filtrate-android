package com.reactiverobot.filtrate;

import android.widget.CheckBox;
import android.widget.EditText;

import com.reactiverobot.filtrate.prefs.FiltratePrefs;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, emulateSdk = 21)
public class MainActivityTest {

    @Test
    public void testActivityStarts() {
        MainActivity mainActivity = Robolectric.setupActivity(MainActivity.class);

        Assert.assertNotNull(mainActivity.findViewById(R.id.at_string_view));
        Assert.assertNotNull(mainActivity.findViewById(R.id.user_name_input));
        Assert.assertNotNull(mainActivity.findViewById(R.id.filter_enabled));
    }

    @Test
    public void testShadowApplicationContextSharesPrefsWithActivity() {
        FiltratePrefs shadowPrefs = new FiltratePrefs(new ShadowActivity().getApplicationContext());
        shadowPrefs.enabled(true);
        shadowPrefs.username("test");

        FiltratePrefs activityPrefs = new FiltratePrefs(Robolectric.setupActivity(MainActivity.class));

        assertEquals(shadowPrefs.enabled(), activityPrefs.enabled());
        assertEquals(shadowPrefs.username(), activityPrefs.username());
    }

    @Test
    public void testUsernameSetFromPrefs() {
        String testUsername = "testname";
        FiltratePrefs shadowPrefs = new FiltratePrefs(new ShadowActivity().getApplicationContext());
        shadowPrefs.username(testUsername);

        MainActivity mainActivity = Robolectric.setupActivity(MainActivity.class);
        EditText usernameInput = (EditText) mainActivity.findViewById(R.id.user_name_input);
        assertEquals(testUsername, usernameInput.getText().toString());
    }

    @Test
    public void testUsernamePrefsSetFromUsernameInput() {
        String testUsername = "testname";
        MainActivity mainActivity = Robolectric.setupActivity(MainActivity.class);

        assertEquals("", new FiltratePrefs(mainActivity).username());

        EditText usernameInput = (EditText) mainActivity.findViewById(R.id.user_name_input);
        usernameInput.setText(testUsername);

        assertEquals(testUsername, new FiltratePrefs(mainActivity).username());
    }

    @Test
    public void testEnabledSetFromPrefs() {
        FiltratePrefs shadowPrefs = new FiltratePrefs(new ShadowActivity().getApplicationContext());
        shadowPrefs.enabled(true);

        MainActivity mainActivity = Robolectric.setupActivity(MainActivity.class);
        CheckBox enabledView = (CheckBox) mainActivity.findViewById(R.id.filter_enabled);

        assertEquals(true, enabledView.isChecked());
    }

    @Test
    public void testEnabledPrefsSetFromEnabledCheckbox() {
        MainActivity mainActivity = Robolectric.setupActivity(MainActivity.class);

        assertFalse(new FiltratePrefs(mainActivity).enabled());

        CheckBox enabledCheckbox = (CheckBox) mainActivity.findViewById(R.id.filter_enabled);
        enabledCheckbox.performClick();

        assertTrue(new FiltratePrefs(mainActivity).enabled());
    }
}
