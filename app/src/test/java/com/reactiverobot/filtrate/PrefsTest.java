package com.reactiverobot.filtrate;

import com.reactiverobot.filtrate.BuildConfig;
import com.reactiverobot.filtrate.MainActivity;
import com.reactiverobot.filtrate.prefs.FiltratePrefs;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, emulateSdk = 21)
public class PrefsTest {

    private MainActivity mainActivity;

    @Before
    public void setup() {
        mainActivity = Robolectric.setupActivity(MainActivity.class);
    }

    @Test
    public void testDefaultEnabledFalse() {
        assertFalse(new FiltratePrefs(mainActivity).enabled());
    }

    @Test
    public void testSetEnabledPref() {
        FiltratePrefs prefs = new FiltratePrefs(mainActivity);
        prefs.enabled(true);

        assertTrue(new FiltratePrefs(mainActivity).enabled());
    }

    @Test
    public void testDefaultUsernameEmpty() {
        assertEquals("", new FiltratePrefs(mainActivity).username());
    }

    @Test
    public void testSetUsername() {
        FiltratePrefs prefs = new FiltratePrefs(mainActivity);
        prefs.username("matt");

        assertEquals("matt", new FiltratePrefs(mainActivity).username());
    }
}
